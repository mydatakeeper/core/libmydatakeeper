#include "arguments.h"

#include <iostream>
#include <iomanip>
#include <getopt.h>

using namespace mydatakeeper;

namespace {

void display_version(const char* arg0, const char* version)
{
    cout << arg0 << endl;
    cout << "\tversion: " << version << endl;

    exit(0);
}

void display_usage(const char* arg0, const vector<Option> &opts, int err = 0, const char* msg = nullptr)
{
    if (msg)
        cout << arg0 << ": " << msg << endl;

    cout << "usage: " << arg0 << " [options ...]" << endl;
    cout << endl;
    cout << "options: " << endl;

    vector<string> labels;
    size_t longest = 12; // length of "--version,-v"
    for(auto& opt : opts)
    {
        string label;
        if (!opt.long_name.empty()){
            label = "--" + opt.long_name;
        }
        if (opt.short_name) {
            if (!label.empty())
                label += ',';
            label += "-";
            label += string(1, opt.short_name);
        }
        labels.push_back(label);
        longest = max(longest, label.size());
    }

    for(size_t i = 0; i < opts.size(); ++i) {
        cout << '\t' << setw(longest) << left << labels[i] << '\t' << opts[i].description << endl;
    }
    cout << '\t' << setw(longest) << left << "--version,-v" << '\t' << "Display the current version and exit" << endl;
    cout << '\t' << setw(longest) << left << "--help,-h" << '\t' << "Display this help message and exit" << endl;

    exit(err);
}

const Option* find_option(const vector<Option> &opts, int c, int idx)
{
    int count = 0;
    for (auto& opt : opts) {
        if (c == 0) {
            if (opt.long_name.empty())
                continue;
            if (count++ == idx)
                return &opt;
        } else if (opt.short_name == c) {
            return &opt;
        }
    }

    return nullptr;
}

} /* end namespace */

namespace mydatakeeper {

void parse_arguments(int argc, char** argv, const char* version, const vector<Option> &opts)
{
    string msg;
    string short_options;
    vector<struct option> long_options;
    for (auto& opt : opts) {
        if (!opt.default_value.empty())
            opt.value = opt.default_value;

        if (!opt.long_name.empty()) {
            long_options.push_back({
                opt.long_name.c_str(),
                opt.flag,
                0,
                0
            });
        }

        if (opt.short_name) {
            short_options += opt.short_name;
            if (opt.flag == required_argument)
                short_options += ':';
            else if (opt.flag == optional_argument)
                short_options += "::";
        }
    }
    long_options.push_back({"version", no_argument, 0, 0});
    long_options.push_back({"help", no_argument, 0, 0});
    long_options.push_back({0, 0, 0, 0});
    short_options += "vh";

    while (1) {
        int idx;
        int c = getopt_long(argc, argv, short_options.data(), long_options.data(), &idx);
        if (c == -1)
            break;

        if (c == 'v' || idx == (int)opts.size())
            display_version(argv[0], version);

        if (c == 'h' || idx == (int)opts.size() + 1)
            display_usage(argv[0], opts);

        if (c == '?')
            display_usage(argv[0], opts, 2);

        const Option* opt = find_option(opts, c, idx);
        if (!opt) {
            msg = string("unkown parameter");
            display_usage(argv[0], opts, 2, msg.c_str());
        }

        string value = optarg ? optarg : "";
        if (opt->validator) {
            try {
                opt->validator(opt->long_name, value);
            } catch (exception& e) {
                display_usage(argv[0], opts, 2, e.what());
            }
        }

        opt->value = value;
    }

    if (optind < argc) {
        while (optind < argc) {
            if (!msg.empty())
                msg += '\n';
            msg += string("unkown parameter '") + argv[optind++] + "'";
        }
        display_usage(argv[0], opts, 2, msg.c_str());
    }

    for (auto& opt : opts) {
        if (opt.flag == required_argument && opt.value.empty()) {
            if (!msg.empty())
                msg += '\n';
            msg += string("missing value for '") + (opt.long_name.empty() ?
                string(1, opt.short_name) : opt.long_name) + "'";
        }
    }
    if (!msg.empty())
        display_usage(argv[0], opts, 2, msg.c_str());
}

} /* end namespace mydatakeeper */
