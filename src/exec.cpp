#include "exec.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <map>
#include <algorithm>

namespace {
struct pipes {
    struct {
        int read;
        int write;
    } out, err;
};
std::map<pid_t, struct pipes> fds;
}

pid_t execute_async(const string &binary, const vector<string> &args)
{
    struct pipes fd;
    if (pipe((int*)&fd.out) == -1 || pipe((int*)&fd.err) == -1) {
        perror("pipe()");
        return -1;
    }

    pid_t pid = fork();
    if (pid == -1) {
        perror("fork()");
        return -1;
    }

    if (pid == 0) { // Child process
        if (close(fd.out.read) == -1 || close(fd.err.read) == -1) {
            perror("pipe()");
            return -1;
        }
        if (dup2(fd.out.write, STDOUT_FILENO) == -1 || dup2(fd.err.write, STDERR_FILENO) == -1) {
            perror("dup2()");
            return -1;
        }

        vector<const char*> c_args = {binary.c_str()};
        for (auto& arg : args)
            c_args.push_back(arg.c_str());
        c_args.push_back(nullptr);
        execvp(binary.c_str(), (char * const *)c_args.data());
        perror("execvp()");
        return -1;
    }

    // Parent process
    if (close(fd.out.write) == -1 || close(fd.err.write) == -1) {
        perror("pipe()");
        return -1;
    }

    fds[pid] = fd;
    return pid;
}

int read(pid_t pid, string *out, string *err)
{
    char buffer[1024];
    int len = 0;

    auto search = find_if(fds.begin(), fds.end(), [&pid](const auto& it) {
        return it.first == pid;
    });
    if (search == fds.end()) {
        perror("find_if()");
        return -1;
    }
    struct pipes fd = fds.at(pid);

    do {
        len = read(fd.out.read, buffer, sizeof(buffer));
        if (len == -1) {
            perror("read()");
            return -1;
        }
        if (out) out->append(buffer, len);
    } while(len == sizeof(buffer));
    if (out) out->append("\0");

    do {
        len = read(fd.err.read, buffer, sizeof(buffer));
        if (len == -1) {
            perror("read()");
            return -1;
        }
        if (err) err->append(buffer, len);
    } while(len == sizeof(buffer));
    if (err) err->append("\0");

    return 0;
}

int join(pid_t pid, string *out, string *err, uint8_t *sig)
{
    int status = 0;
    do {
        auto wpid = waitpid(pid, &status, WUNTRACED);
        if (wpid == -1) {
            perror("waitpid()");
            return -1;
        }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));

    if (read(pid, out, err) == -1) {
        return -1;
    } else {
        close(fds.at(pid).out.read);
        close(fds.at(pid).err.read);
        fds.erase(pid);
    }

    /* Exited normally */
    if (WIFEXITED(status))
        return WEXITSTATUS(status);

    /* Exited after receiving an unhandled signal */
    if (sig)
        *sig = WTERMSIG(status);

    return -1;
}

int execute(const string &binary, const vector<string> &args,
    string *out, string *err, uint8_t *sig)
{
    pid_t pid = execute_async(binary, args);
    return join(pid, out, err, sig);
}

