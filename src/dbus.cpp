#include "dbus.h"

#include <string>

template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<uint8_t>(
    const string &name, const uint8_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<bool>(
    const string &name, const bool b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<int16_t>(
    const string &name, const int16_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<uint16_t>(
    const string &name, const uint16_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<int32_t>(
    const string &name, const int32_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<uint32_t>(
    const string &name, const uint32_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<int64_t>(
    const string &name, const int64_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<uint64_t>(
    const string &name, const uint64_t b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<double>(
    const string &name, const double b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<string>(
    const string &name, const string b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<DBus::Path>(
    const string &name, const DBus::Path b, bool can_read, bool can_write);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<DBus::Signature>(
    const string &name, const DBus::Signature b, bool can_read, bool can_write);
template<> DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::with<DBus::Variant>(
    const string &name, const DBus::Variant v, bool can_read, bool can_write)
{
    return _with(name, v.signature(), v, can_read, can_write);
}

DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::_with(
    const string &name, const string sig, const DBus::Variant val, bool can_read, bool can_write)
{
    DBus::InterfaceAdaptor::_properties[name].read = can_read;
    DBus::InterfaceAdaptor::_properties[name].write = can_write;
    DBus::InterfaceAdaptor::_properties[name].value = val;
    DBus::InterfaceAdaptor::_properties[name].sig = sig;

    properties.pop_back();
    properties.push_back({
        strdup(name.c_str()),
        DBus::InterfaceAdaptor::_properties[name].sig.c_str(),
        can_read,
        can_write
    });
    properties.push_back({0, 0});

    return *this;
}

template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<uint8_t>(
    const string &name, const uint8_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<bool>(
    const string &name, const bool b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<int16_t>(
    const string &name, const int16_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<uint16_t>(
    const string &name, const uint16_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<int32_t>(
    const string &name, const int32_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<uint32_t>(
    const string &name, const uint32_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<int64_t>(
    const string &name, const int64_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<uint64_t>(
    const string &name, const uint64_t b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<double>(
    const string &name, const double b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<string>(
    const string &name, const string b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<DBus::Path>(
    const string &name, const DBus::Path b);
template DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<DBus::Signature>(
    const string &name, const DBus::Signature b);
template<> DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::set<DBus::Variant>(
    const string &name, const DBus::Variant v)
{
    return _set(name, v.signature(), v);
}

DynamicInterfaceAdaptor &DynamicInterfaceAdaptor::_set(
    const string &name, const string sig, const DBus::Variant val)
{
    auto& props = DBus::InterfaceAdaptor::_properties;
    if (props.find(name) == props.end())
        throw ;

    if (sig != props.at(name).sig)
        throw ;

    props.at(name).value = val;

    return *this;

}

template <>
struct DynamicProperty<DBus::Variant>
{
    static DBus::Variant to_variant(const DBus::Variant &v)
    {
        return v;
    }

    static std::string sig()
    {
        return DBus::type<DBus::Variant>::sig();
    }
};


namespace fr {
namespace mydatakeeper {
extern const string ApplicationsName("fr.mydatakeeper.Applications");
extern const string ApplicationsPath("/fr/mydatakeeper/Applications");
extern const string ApplicationsInterface("fr.mydatakeeper.Applications");
extern const string ApplicationName("fr.mydatakeeper.Application");
extern const string ApplicationPath("/fr/mydatakeeper/Application");
extern const string ApplicationInterface("fr.mydatakeeper.Application");
extern const string InterfaceName("fr.mydatakeeper.Interface");
extern const string InterfacePath("/fr/mydatakeeper/Interface");
extern const string InterfaceInterface("fr.mydatakeeper.Interface");
extern const string ConfigName("fr.mydatakeeper.Applications");
extern const string ConfigInterface("fr.mydatakeeper.Config");
} /* end namespace mydatakeeper */
} /* end namespace fr */

namespace org {
namespace freedesktop {
extern const string locale1Path("/org/freedesktop/locale1");
extern const string locale1Name("org.freedesktop.locale1");
extern const string timedate1Path("/org/freedesktop/timedate1");
extern const string timedate1Name("org.freedesktop.timedate1");
extern const string login1Name("org.freedesktop.login1");
extern const string login1Path("/org/freedesktop/login1");
extern const string systemd1Name("org.freedesktop.systemd1");
extern const string systemd1Path("/org/freedesktop/systemd1");
} /* end namespace freedesktop */
} /* end namespace org */
