#include <curl/curl.h>

#include "download.h"

#include <string>

using namespace std;

namespace {

static size_t write_buffer(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

}; // end namespace

bool download_file(const string& url, string& buffer, string* error)
{
    static const string method("GET");
    return download_file(url, method, buffer, error);
}

bool download_file(const string& url, const string& method, string& buffer,
    string* error)
{
    return download_file(url, method, {}, buffer, error);
}

bool download_file(const string& url, const string& method,
    const vector<string>& headers,
    string& buffer, string* error)
{
    return download_file(url, method, headers, "", buffer, error);
}

bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data,
    string& buffer, string* error)
{
    return download_file(url, method, headers, data, "/dev/null", buffer, error);
}

bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data, const string& cookie_jar,
    string& buffer, string* error)
{
    return download_file(url, method, headers, data, cookie_jar, true, buffer, error);
}

bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data, const string& cookie_jar,
    bool redirect, string& buffer, string* error)
{
    CURLcode code;
    struct curl_slist *header_list = nullptr, *new_header_list = nullptr;
    curl_version_info_data *version_data = curl_version_info(CURLVERSION_NOW);
    string user_agent = string("curl/") + version_data->version;

    buffer.clear();
    curl_global_init(CURL_GLOBAL_DEFAULT);

    CURL *conn = curl_easy_init();
    if (!conn) {
        code = CURLE_FAILED_INIT;
        if (error) *error = "Unable to start a libcurl easy session";
        goto global_cleanup;
    }

    // Set HTTP method
    code = curl_easy_setopt(conn, CURLOPT_CUSTOMREQUEST, method.c_str());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Set targeted URL
    code = curl_easy_setopt(conn, CURLOPT_URL, url.c_str());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Set UserAgent header
    code = curl_easy_setopt(conn, CURLOPT_USERAGENT, user_agent.c_str());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Set custom headers
    if (!headers.empty()) {
        for (auto& header : headers) {
            new_header_list = curl_slist_append(header_list, header.c_str());
            if (new_header_list == NULL) {
                if (error) *error = "Unable to add headers to header list";
                curl_slist_free_all(header_list);
                goto cleanup;
            }
            header_list = new_header_list;
        }
        code = curl_easy_setopt(conn, CURLOPT_HTTPHEADER, header_list);
        if(CURLE_OK != code) {
            if (error) *error = curl_easy_strerror(code);
            curl_slist_free_all(header_list);
            goto cleanup;
        }
    }

    // Set request data
    code = curl_easy_setopt(conn, CURLOPT_POSTFIELDSIZE, data.size());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    code = curl_easy_setopt(conn, CURLOPT_POSTFIELDS, data.c_str());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Set cookie jar
    code = curl_easy_setopt(conn, CURLOPT_COOKIEJAR, cookie_jar.c_str());
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Follow redirections
    code = curl_easy_setopt(conn, CURLOPT_FOLLOWLOCATION, redirect ? 1L : 0L);
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Set callback functions
    code = curl_easy_setopt(conn, CURLOPT_WRITEFUNCTION, write_buffer);
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    code = curl_easy_setopt(conn, CURLOPT_WRITEDATA, &buffer);
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

    // Execute request
    code = curl_easy_perform(conn);
    if(CURLE_OK != code) {
        if (error) *error = curl_easy_strerror(code);
        goto cleanup;
    }

cleanup:
    curl_slist_free_all(header_list);
    curl_easy_cleanup(conn);

global_cleanup:
    curl_global_cleanup();

    return code == CURLE_OK;
}
