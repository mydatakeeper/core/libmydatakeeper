#include "config.h"

#include <dbus-c++/dbus.h>

const string name("fr.mydatakeeper.Test");
const string path("/fr/mydatakeeper/test");

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SessionBus();
    fr::mydatakeeper::ConfigProxy config(conn, path, name);

    /* Getters */
    auto y = config.get("y");
    auto b = config.get("b");
    auto n = config.get("n");
    auto q = config.get("q");
    auto i = config.get("i");
    auto u = config.get("u");
    auto x = config.get("x");
    auto t = config.get("t");
    auto d = config.get("d");

    auto s = config.get("s");
    auto o = config.get("o");
    auto g = config.get("g");

    auto ybnds = config.get("ybnds");
    auto as = config.get("as");
    auto ais = config.get("ais");
    auto var = config.get("v");

    /* Setters */
    config.set<uint8_t>("y", 1);
    config.set<bool>("b", true);
    config.set<int16_t>("n", 1);
    config.set<uint16_t>("q", 1);
    config.set<int32_t>("i", 1);
    config.set<uint32_t>("u", 1);
    config.set<int64_t>("x", 1);
    config.set<uint64_t>("t", 1);
    config.set<double>("d", 1.0);

    config.set<string>("s", "Hello world");
    config.set<DBus::Path>("o", "/usr/bin/true");
    config.set<DBus::Signature>("g", "a{ss}");

    config.set<tuple<uint8_t, bool, int16_t, double, string>>("ybnds", {1, true, 1, 1.0, "Hello world"});
    config.set<vector<string>>("as", {"Hello", "World"});
    config.set<map<int32_t, string>>("ais", {{1, "Hello"}, {2, "World"}});
    DBus::Variant v;
    v.writer().append_string("Hello variant");
    config.set<DBus::Variant>("v", v);
}