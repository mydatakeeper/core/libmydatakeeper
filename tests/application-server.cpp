#include "application.h"

#include <dbus-c++/dbus.h>

const string name("fr.mydatakeeper.Test");
const string path("/fr/mydatakeeper/test");

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SessionBus();
    fr::mydatakeeper::ApplicationAdaptor application(conn, path);
    fr::mydatakeeper::Application_adaptor &app = application;

    app.name = "Test app";
    app.version = "0.0.1";
    app.admin = false;
    app.contact = "contact@example.com";
    app.website = "example.com";
    app.description = "A dummy test application";
    app.tags = std::vector< std::string >();
    app.categories = std::vector< std::string >();
    app.icons = std::map< std::string, std::string >();
    app.fields = std::map< std::string, std::map< std::string, ::DBus::Variant > >();

    conn.request_name(name.c_str());

    dispatcher.enter();
}