#include "application.h"

#include <dbus-c++/dbus.h>

const string name("fr.mydatakeeper.Test");
const string path("/fr/mydatakeeper/test");
const string interface("fr.mydatakeeper.Application");

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SessionBus();
    fr::mydatakeeper::ApplicationProxy app(conn, path, name);

    /* Getters */
    auto has_config = app.HasConfig();
    auto has_interface = app.HasInterface();

    app.Reset();

    auto icon = app.GetIcon("40x40");

    app.GetAll(interface);
}