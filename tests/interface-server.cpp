#include "interface.h"

#include <dbus-c++/dbus.h>

const string name("fr.mydatakeeper.Test");
const string path("/fr/mydatakeeper/test");

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SessionBus();
    fr::mydatakeeper::InterfaceAdaptor application(conn, path);
    fr::mydatakeeper::Interface_adaptor &app = application;

    app.root = "/tmp/";
    app.index = "index.html";
    app.files = std::vector< std::string >();
    app.methods = std::map< std::string, std::vector< std::map< std::string, std::string > > >();

    conn.request_name(name.c_str());

    dispatcher.enter();
}