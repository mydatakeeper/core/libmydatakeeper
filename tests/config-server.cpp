#include "config.h"

#include <dbus-c++/dbus.h>

const string name("fr.mydatakeeper.Test");
const string path("/fr/mydatakeeper/test");


DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SessionBus();
    fr::mydatakeeper::ConfigAdaptor config(conn, path);

    config.with<uint8_t>("y", 0);
    config.with<bool>("b", false);
    config.with<int16_t>("n", 0);
    config.with<uint16_t>("q", 0);
    config.with<int32_t>("i", 0);
    config.with<uint32_t>("u", 0);
    config.with<int64_t>("x", 0);
    config.with<uint64_t>("t", 0);
    config.with<double>("d", 0.0);

    config.with<string>("s", "");
    config.with<DBus::Path>("o", DBus::Path("/dev/null"));
    config.with<DBus::Signature>("g", DBus::Signature("s"));

    config.with<tuple<uint8_t, bool, int16_t, double, string>>("ybnds", {0, false, 0, 0.0, ""});
    config.with<vector<string>>("as", {});
    config.with<map<int32_t, string>>("ais", {});
    DBus::Variant v;
    v.writer().append_bool(false);
    config.with<DBus::Variant>("v", v);

    conn.request_name(name.c_str());

    dispatcher.enter();
}