#include "systemd.h"

#include <dbus-c++/dbus.h>

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    DBus::default_dispatcher = &dispatcher;

    DBus::Connection conn = DBus::Connection::SystemBus();

    org::freedesktop::locale1Proxy locale1(conn);

    org::freedesktop::timedate1Proxy timedate1(conn);

    org::freedesktop::login1::ManagerProxy LoginManager(conn);
    org::freedesktop::login1::SeatProxy Seat(conn, "/org/freedesktop/login1/seat/seat0");
    org::freedesktop::login1::UserProxy User(conn, "/org/freedesktop/login1/user/self");
    org::freedesktop::login1::SessionProxy Session(conn, "/org/freedesktop/login1/session/self");

    org::freedesktop::systemd1::ManagerProxy UnitManager(conn);
    org::freedesktop::systemd1::UnitProxy Unit(conn, "/org/freedesktop/systemd1/unit/avahi_2ddaemon_2eservice");
    org::freedesktop::systemd1::ServiceProxy Service(conn, "/org/freedesktop/systemd1/unit/avahi_2ddaemon_2eservice");
    org::freedesktop::systemd1::SocketProxy Socket(conn, "/org/freedesktop/systemd1/unit/avahi_2ddaemon_2esocket");
    org::freedesktop::systemd1::TargetProxy Target(conn, "/org/freedesktop/systemd1/unit/basic_2etarget");
    org::freedesktop::systemd1::DeviceProxy Device(conn, "/org/freedesktop/systemd1/unit/dev_2ddisk_2dby_5cx2did_2data_5cx2dSAMSUNG_5fHD501LJ_5fS0MUJ1KQ161445_2edevice");
    org::freedesktop::systemd1::MountProxy Mount(conn, "/org/freedesktop/systemd1/unit/home_2emount");
    org::freedesktop::systemd1::AutomountProxy Automount(conn, "/org/freedesktop/systemd1/unit/proc_2dsys_2dfs_2dbinfmt_5fmisc_2eautomount");
    org::freedesktop::systemd1::SnapshotProxy Snapshot(conn, "/org/freedesktop/systemd1/unit/foo_2esnapshot");
    org::freedesktop::systemd1::TimerProxy Timer(conn, "/org/freedesktop/systemd1/unit/systemd_2dtmpfiles_2dclean_2etimer");
    org::freedesktop::systemd1::SwapProxy Swap(conn, "/org/freedesktop/systemd1/unit/dev_2dsda3_2eswap");
    org::freedesktop::systemd1::PathProxy Path(conn, "/org/freedesktop/systemd1/unit/cups_2epath");
    org::freedesktop::systemd1::SliceProxy Slice(conn, "/org/freedesktop/systemd1/unit/system_2eslice");
    org::freedesktop::systemd1::ScopeProxy Scope(conn, "/org/freedesktop/systemd1/unit/session_2d1_2escope");
    org::freedesktop::systemd1::JobProxy Job(conn, "/org/freedesktop/systemd1/job/1292");
}