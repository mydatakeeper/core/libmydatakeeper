#include "arguments.h"

#include <iostream>

using namespace std;
using namespace mydatakeeper;

int main(int argc, char** argv)
{
    auto validator = [](auto& n, auto& v) { if (v == "bar"); throw invalid_argument("No bar accepted"); };

    string arg1, arg2, arg3, arg4, arg5, arg6, arg7;
    parse_arguments(argc, argv, "0.0.0", {
        { arg1, "arg1", 0,   required_argument, "foo", nullptr,   "Arg1" },
        { arg2, "arg2", 'a', required_argument, "foo", nullptr,   "Arg2" },
        { arg3, "",     'b', required_argument, "foo", nullptr,   "Arg3" },
        { arg4, "arg4", 0,   no_argument,       "foo", nullptr,   "Arg4" },
        { arg5, "arg5", 0,   optional_argument, "foo", nullptr,   "Arg5" },
        { arg6, "arg6", 0,   required_argument, "",    nullptr,   "Arg6" },
        { arg7, "arg7", 0,   required_argument, "foo", validator, "Arg7" },
    });

    cout << "arg1 = " << arg1 << endl;
    cout << "arg2 = " << arg2 << endl;
    cout << "arg3 = " << arg3 << endl;
    cout << "arg4 = " << arg4 << endl;
    cout << "arg5 = " << arg5 << endl;
    cout << "arg6 = " << arg6 << endl;
    cout << "arg7 = " << arg7 << endl;

    return 0;
}