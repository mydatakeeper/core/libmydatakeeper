#ifndef __MDK_LIB_CONFIG__
#define __MDK_LIB_CONFIG__

#include "dbus.h"

using namespace std;

namespace fr {
namespace mydatakeeper {

const string ConfigName("fr.mydatakeeper.Applications");
const string ConfigInterface("fr.mydatakeeper.Config");

struct ConfigProxy
: public DynamicInterfaceProxy,
  public DBusProxy
{
    ConfigProxy(
        DBus::Connection &connection,
        const string &path,
        const string &name = ConfigName,
        const string &interface = ConfigInterface)
        :  DynamicInterfaceProxy(interface),
           DBusProxy(connection, path, name)
    {}
};

struct ConfigAdaptor
: public DynamicInterfaceAdaptor,
  public DBusAdaptor
{
    ConfigAdaptor(
        DBus::Connection &connection,
        const string &path,
        const string &interface = ConfigInterface)
        : DynamicInterfaceAdaptor(interface),
          DBusAdaptor(connection, path)
    {}
};

} /* end namespace mydatakeeper */
} /* end namespace fr */

#endif /* __MDK_LIB_CONFIG__ */
