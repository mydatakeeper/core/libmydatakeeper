#ifndef __MDK_LIB_APPLICATIONS__
#define __MDK_LIB_APPLICATIONS__

#include "dbus.h"

#include "proxy/fr.mydatakeeper.Applications.h"
#include "adaptor/fr.mydatakeeper.Applications.h"

using namespace std;

namespace fr {
namespace mydatakeeper {

extern const string ApplicationsName;
extern const string ApplicationsPath;
extern const string ApplicationsInterface;

struct ApplicationsProxy
: public GenericDBusProxyWithPath<Applications_proxy, ApplicationsPath, ApplicationsName>
{
    using GenericDBusProxyWithPath::GenericDBusProxyWithPath;

    virtual void InstallChange(
        const std::string& app_name,
        const std::string& state,
        const DBus::Variant& meta)
    {
        if (nullptr != onInstallChange)
            onInstallChange(app_name, state, meta);
    }

    typedef void (InstallChange_t)(
        const string&,
        const std::string&,
        const DBus::Variant&
    );
    function<InstallChange_t> onInstallChange = nullptr;
};

struct ApplicationsAdaptor
: public GenericDBusAdaptorWithPath<Applications_adaptor, ApplicationsPath>
{
    using GenericDBusAdaptorWithPath::GenericDBusAdaptorWithPath;
};

} /* end namespace mydatakeeper */
} /* end namespace fr */

#endif /* __MDK_LIB_APPLICATIONS__ */
