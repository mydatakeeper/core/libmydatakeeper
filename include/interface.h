#ifndef __MDK_LIB_INTERFACE__
#define __MDK_LIB_INTERFACE__

#include "dbus.h"
#include "proxy/fr.mydatakeeper.Interface.h"
#include "adaptor/fr.mydatakeeper.Interface.h"

using namespace std;

namespace fr {
namespace mydatakeeper {

extern const string InterfaceName;
extern const string InterfacePath;
extern const string InterfaceInterface;

struct InterfaceProxy
: public GenericDBusProxy<fr::mydatakeeper::Interface_proxy, InterfaceName>
{
    using GenericDBusProxy::GenericDBusProxy;

    virtual void StateChange(const std::string& state)
    {
        if (nullptr != onStateChange)
            onStateChange(state);
    }

    typedef void (StateChange_t)(const string&);
    function<StateChange_t> onStateChange = nullptr;
};

struct InterfaceAdaptor
: public GenericDBusAdaptor<fr::mydatakeeper::Interface_adaptor>
{
    using GenericDBusAdaptor::GenericDBusAdaptor;
};

} /* end namespace mydatakeeper */
} /* end namespace fr */

#endif /* __MDK_LIB_INTERFACE__ */
