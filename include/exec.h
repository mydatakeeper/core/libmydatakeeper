#ifndef __MDK_API_EXEC__
#define __MDK_API_EXEC__

#include <string>
#include <vector>

using namespace std;

/**
 * Start the execution of a binary as a forked process.
 *
 * binary: The full path to the binary to execute
 * args: A list of arguments to pass to the binary.
 *       The binary path will be prepended
 *
 * return: The PID of the child process or -1 in case of error.
 */
pid_t execute_async(const string &binary, const vector<string> &args = {});

/**
 * Read the stdout and stderr of a forked process.
 *
 * pid: The PID of the running process to read from.
 * out: A pointer to a string to which the PID stdout will be appended; or nullptr.
 * err: A pointer to a string to which the PID stderr will be appended; or nullptr.
 *
 * return: 0 in case of success; -1 otherwise.
 */
int read(pid_t pid, string *out = nullptr, string *err = nullptr);

/**
 * Wait for the end of the execution of a forked process.
 *
 * pid: The PID of the running process to wait for.
 * out: A pointer to a string to which the PID stdout will be appended; or nullptr.
 * err: A pointer to a string to which the PID stderr will be appended; or nullptr.
 * sig: The signal received by the process if terminated because of a signal.
 *
 * return: The process exit status in case of success;
 *         -1 in case of error or if signal-terminated.
 */
int join(pid_t pid, string *out = nullptr, string *err = nullptr, uint8_t *sig = nullptr);

/**
 * Execute a binary as a forked process.
 *
 * binary: The full path to the binary to execute
 * args: A list of arguments to pass to the binary.
 *       The binary path will be prepended
 * out: A pointer to a string to which the PID stdout will be appended; or nullptr.
 * err: A pointer to a string to which the PID stderr will be appended; or nullptr.
 * sig: The signal received by the process if terminated because of a signal.
 *
 * return: The process exit status in case of success;
 *         -1 in case of error or if signal-terminated.
 */
int execute(const string &binary, const vector<string> &args = {},
    string *out = nullptr, string *err = nullptr, uint8_t *sig = nullptr);

#endif /* __MDK_API_EXEC__ */
