#ifndef __MDK_API_DOWNLOAD__
#define __MDK_API_DOWNLOAD__

#include <string>
#include <vector>

using namespace std;

bool download_file(const string& url, string& buffer, string* error = nullptr);
bool download_file(const string& url, const string& method, string& buffer,
    string* error = nullptr);
bool download_file(const string& url, const string& method,
    const vector<string>& headers,
    string& buffer, string* error = nullptr);
bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data,
    string& buffer, string* error = nullptr);
bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data, const string& cookie_jar,
    string& buffer, string* error = nullptr);
bool download_file(const string& url, const string& method,
    const vector<string>& headers, const string& data, const string& cookie_jar,
    bool redirect, string& buffer, string* error = nullptr);

#endif /* __MDK_API_DOWNLOAD__ */
