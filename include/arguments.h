#ifndef __MDK_LIB_ARGUMENTS__
#define __MDK_LIB_ARGUMENTS__

#include <string>
#include <vector>
#include <functional>

using namespace std;

namespace mydatakeeper {

enum OptionFlag
{
    no_argument = 0,
    required_argument = 1,
    optional_argument = 2,
};

struct Option
{
    string &value;
    const string &long_name;
    const char short_name;
    const int flag;
    const string& default_value;
    function<void (const string&, const string&)> validator;
    const string& description;
};

void parse_arguments(int argc, char** argv, const char* version, const vector<Option> &opts);

} /* end namespace mydatakeeper */

#endif /* __MDK_LIB_ARGUMENTS__ */
