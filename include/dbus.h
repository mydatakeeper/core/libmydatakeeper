#ifndef __MDK_LIB_DBUS__
#define __MDK_LIB_DBUS__

#include <functional>
#include <cstring>

#include <dbus-c++/property.h>
#include <dbus-c++/introspection.h>
#include <dbus-c++/object.h>

#include "tuple.h"

using namespace std;

struct DBusProxy
: public DBus::PropertiesProxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
    DBusProxy(DBus::Connection &connection, const string &path, const string &name)
        : DBus::ObjectProxy(connection, path.c_str(), name.c_str())
    {}

    virtual void PropertiesChanged(
        const string& interface_name,
        const map<string, DBus::Variant>& changed_properties,
        const vector<string>& invalidated_properties)
    {
        if (nullptr != onPropertiesChanged) {
            onPropertiesChanged(interface_name, changed_properties, invalidated_properties);
        }
    }

    typedef void (PropertiesChanged_t)(
        const string&,
        const map<string, DBus::Variant>&,
        const vector<string>&
    );

    function<PropertiesChanged_t> onPropertiesChanged = nullptr;
};

struct DBusAdaptor
: public DBus::PropertiesAdaptor,
  public DBus::IntrospectableAdaptor,
  public DBus::ObjectAdaptor
{
    DBusAdaptor(DBus::Connection &connection, const string &path)
        : DBus::ObjectAdaptor(connection, path)
    {}

    virtual void on_get_all_property(InterfaceAdaptor &interface)
    {
        if (nullptr != onGetAll)
            onGetAll(interface.name());
    }

    virtual void on_get_property(InterfaceAdaptor &interface, const std::string &property, DBus::Variant &value)
    {
        if (nullptr != onGet)
            onGet(interface.name(), property, value);
    }

    virtual void on_set_property(InterfaceAdaptor &interface, const std::string &property, const DBus::Variant &value)
    {
        if (nullptr != onSet)
            onSet(interface.name(), property, value);
    }

    typedef void (GetAll_t)(const string&);
    typedef void (Get_t)(const string&, const string&, const DBus::Variant&);
    typedef void (Set_t)(const string&, const string&, const DBus::Variant&);

    function<GetAll_t> onGetAll = nullptr;
    function<Get_t> onGet = nullptr;
    function<Set_t> onSet = nullptr;
};

template <class T, const string& defaultName>
struct GenericDBusProxy
: public DBusProxy,
  public T
{
    GenericDBusProxy(DBus::Connection &connection,
        const string &path,
        const string &name = defaultName)
        : DBusProxy(connection, path, name)
    {}
};

template <class T, const string& defaultPath, const string& defaultName>
struct GenericDBusProxyWithPath
: public DBusProxy,
  public T
{
    GenericDBusProxyWithPath(DBus::Connection &connection,
        const string &path = defaultPath,
        const string &name = defaultName)
        : DBusProxy(connection, path, name)
    {}
};

template <class T>
struct GenericDBusAdaptor
: public DBusAdaptor,
  public T
{
    using DBusAdaptor::DBusAdaptor;
};

template <class T, const string& defaultPath>
struct GenericDBusAdaptorWithPath
: public DBusAdaptor,
  public T
{
    GenericDBusAdaptorWithPath(DBus::Connection &connection,
        const string &path = defaultPath)
        : DBusAdaptor(connection, path)
    {}
};

struct DynamicInterfaceProxy
: public DBus::InterfaceProxy
{
    DynamicInterfaceProxy(const string &interface)
        : DBus::InterfaceProxy(interface)
    {}

    DBus::Variant get(const string& property_name)
    {
        DBus::CallMessage call;
        DBus::MessageIter wi = call.writer();

        wi << name();
        wi << property_name;
        call.interface("org.freedesktop.DBus.Properties");
        call.member("Get");
        DBus::Message ret = invoke_method (call);

        DBus::MessageIter ri = ret.reader();
        DBus::Variant argout;
        ri >> argout;
        return argout;
    }

    template <typename T>
    void set(const string& property_name, const T &t)
    {
        DBus::CallMessage call;
        DBus::MessageIter wi = call.writer();

        wi << name();
        wi << property_name;
        wi << DynamicProperty<T>::to_variant(t);
        call.interface("org.freedesktop.DBus.Properties");
        call.member("Set");
        DBus::Message ret = invoke_method (call);
    }
};

class DynamicInterfaceAdaptor
: public DBus::InterfaceAdaptor
{
public:
    DynamicInterfaceAdaptor(const string &interface)
        : DBus::InterfaceAdaptor(interface)
    {
        properties.push_back({0, 0});
    }

    DBus::IntrospectedInterface *introspect() const 
    {
        static ::DBus::IntrospectedMethod methods[] = 
        {
            { 0, 0 }
        };
        static ::DBus::IntrospectedMethod signals[] = 
        {
            { 0, 0 }
        };
        static DBus::IntrospectedInterface interface = 
        {
            DBus::InterfaceAdaptor::name().c_str(),
            methods,
            signals,
            properties.data()
        };

        return &interface;
    }

    template <typename T>
    DynamicInterfaceAdaptor &with(const string &name, const T t, bool can_read = true, bool can_write = true)
    {
        return _with(
            name,
            DynamicProperty<T>::sig(),
            DynamicProperty<T>::to_variant(t),
            can_read,
            can_write
        );
    }

    template <typename T>
    DynamicInterfaceAdaptor &set(const string &name, const T t)
    {
        return _set(
            name,
            DynamicProperty<T>::sig(),
            DynamicProperty<T>::to_variant(t)
        );
    }

    const DBus::Variant &get(const string& name) const
    {
        auto& props = DBus::InterfaceAdaptor::_properties;
        if (props.find(name) == props.end())
            throw ;

        return props.at(name).value;
    }

private:
    vector<DBus::IntrospectedProperty> properties;

    DynamicInterfaceAdaptor &_with(const string &name, const string sig, const DBus::Variant val, bool can_read = true, bool can_write = true);
    DynamicInterfaceAdaptor &_set(const string &name, const string sig, const DBus::Variant val);

};

#endif /* __MDK_LIB_DBUS__ */
