#ifndef __MDK_LIB_APPLICATION__
#define __MDK_LIB_APPLICATION__

#include "dbus.h"
#include "proxy/fr.mydatakeeper.Application.h"
#include "adaptor/fr.mydatakeeper.Application.h"

using namespace std;

namespace fr {
namespace mydatakeeper {

extern const string ApplicationName;
extern const string ApplicationPath;
extern const string ApplicationInterface;

struct ApplicationProxy
: public GenericDBusProxy<fr::mydatakeeper::Application_proxy, ApplicationName>
{
    using GenericDBusProxy::GenericDBusProxy;

    virtual void StateChange(const std::string& state)
    {
        if (nullptr != onStateChange)
            onStateChange(state);
    }

    typedef void (StateChange_t)(const string&);
    function<StateChange_t> onStateChange = nullptr;
};

struct ApplicationAdaptor
: public GenericDBusAdaptor<fr::mydatakeeper::Application_adaptor>
{
    using GenericDBusAdaptor::GenericDBusAdaptor;
};

} /* end namespace mydatakeeper */
} /* end namespace fr */

#endif /* __MDK_LIB_APPLICATION__ */
