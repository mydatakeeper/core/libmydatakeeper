#ifndef __MDK_LIB_SYSTEMD__
#define __MDK_LIB_SYSTEMD__

#include "dbus.h"

/* Proxy interfaces */
#include <dbus-c++/proxy/org.freedesktop.locale1.h>

#include <dbus-c++/proxy/org.freedesktop.timedate1.h>

#include <dbus-c++/proxy/org.freedesktop.login1.Manager.h>
#include <dbus-c++/proxy/org.freedesktop.login1.Seat.h>
#include <dbus-c++/proxy/org.freedesktop.login1.User.h>
#include <dbus-c++/proxy/org.freedesktop.login1.Session.h>

#include <dbus-c++/proxy/org.freedesktop.systemd1.Manager.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Unit.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Service.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Socket.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Target.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Device.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Mount.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Automount.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Snapshot.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Timer.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Swap.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Path.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Slice.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Scope.h>
#include <dbus-c++/proxy/org.freedesktop.systemd1.Job.h>

namespace org {
namespace freedesktop {
extern string locale1Path;
extern string locale1Name;
class locale1Proxy: public GenericDBusProxyWithPath<locale1_proxy, locale1Path, locale1Name>{ using GenericDBusProxyWithPath::GenericDBusProxyWithPath; };

extern string timedate1Path;
extern string timedate1Name;
class timedate1Proxy: public GenericDBusProxyWithPath<timedate1_proxy, timedate1Path, timedate1Name>{ using GenericDBusProxyWithPath::GenericDBusProxyWithPath; };

extern string login1Name;
extern string login1Path;
namespace login1 {
class ManagerProxy: public GenericDBusProxyWithPath<Manager_proxy, login1Path, login1Name>{ using GenericDBusProxyWithPath::GenericDBusProxyWithPath; };
class SeatProxy: public GenericDBusProxy<Seat_proxy, login1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class UserProxy: public GenericDBusProxy<User_proxy, login1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class SessionProxy: public GenericDBusProxy<Session_proxy, login1Name>{ using GenericDBusProxy::GenericDBusProxy; };
} /* end namespace login1 */

extern string systemd1Name;
extern string systemd1Path;
namespace systemd1 {
class ManagerProxy: public GenericDBusProxyWithPath<Manager_proxy, systemd1Path, systemd1Name>{ using GenericDBusProxyWithPath::GenericDBusProxyWithPath; };
class UnitProxy: public GenericDBusProxy<Unit_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class ServiceProxy: public GenericDBusProxy<Service_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class SocketProxy: public GenericDBusProxy<Socket_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class TargetProxy: public GenericDBusProxy<Target_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class DeviceProxy: public GenericDBusProxy<Device_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class MountProxy: public GenericDBusProxy<Mount_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class AutomountProxy: public GenericDBusProxy<Automount_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class SnapshotProxy: public GenericDBusProxy<Snapshot_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class TimerProxy: public GenericDBusProxy<Timer_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class SwapProxy: public GenericDBusProxy<Swap_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class PathProxy: public GenericDBusProxy<Path_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class SliceProxy: public GenericDBusProxy<Slice_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class ScopeProxy: public GenericDBusProxy<Scope_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
class JobProxy: public GenericDBusProxy<Job_proxy, systemd1Name>{ using GenericDBusProxy::GenericDBusProxy; };
} /* end namespace systemd1 */
} /* end namespace freedesktop */
} /* end namespace org */

#endif /* __MDK_LIB_SYSTEMD__ */
