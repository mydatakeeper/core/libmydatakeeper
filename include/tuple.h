#ifndef __MDK_LIB_TUPLE__
#define __MDK_LIB_TUPLE__

#include <tuple>

using namespace std;

template<typename Head, typename ... Tail>
struct dbus_tuple;

template <typename T>
struct DynamicProperty;

template <typename T>
inline DBus::Variant to_variant(const T& t) {
    return DynamicProperty<T>::to_variant(t);
}

template <typename T>
struct DynamicProperty
{
    static DBus::Variant to_variant(const T &t)
    {
        DBus::Variant v;
        DBus::MessageIter wi = v.writer();
        to_variant(wi, t);

        return v;
    }

    static std::string sig()
    {
        return DBus::type<T>::sig();
    }

protected:
    static void to_variant(DBus::MessageIter& wi, const T &t)
    {
        wi << t;
    }
};

template <typename ... ArgsT>
struct DynamicProperty<tuple<ArgsT ...>>
{
    static DBus::Variant to_variant(const tuple<ArgsT ...> &t)
    {
        DBus::Variant v;
        DBus::MessageIter wi = v.writer();
        to_variant(wi, t);

        return v;
    }

    static std::string sig()
    {
        return dbus_tuple<ArgsT ...>::sig();
    }

protected:
    static void to_variant(DBus::MessageIter& wi, const tuple<ArgsT ...> &t)
    {
        dbus_tuple<ArgsT ...>::from_tuple(wi, t);
    }
};

template<typename Head, typename ... Tail>
struct dbus_tuple : public dbus_tuple<Tail ...>
{
public:
    using tuple_type = tuple<Head, Tail ...>;

    static string sig()
    {
        return "(" + _sig() + ")";
    }

    static tuple_type to_tuple(DBus::MessageIter& ri)
    {
        DBus::MessageIter sit = ri.recurse();
        tuple_type t = _to_tuple(sit);
        ++ri;

        return t;
    }

    static void from_tuple(DBus::MessageIter& wi, const tuple_type &t)
    {
        DBus::MessageIter sit = wi.new_struct();
        _from_tuple(sit, t);
        wi.close_container(sit);
    }

protected:
    static string _sig()
    {
        return DynamicProperty<Head>::sig() + dbus_tuple<Tail ...>::_sig();
    }

    static tuple_type _to_tuple(DBus::MessageIter& ri)
    {
        Head t;
        ri >> t;
        return tuple_cat(tuple(t), dbus_tuple<Tail ...>::to_tuple(ri));
    }

    static void _from_tuple(DBus::MessageIter& wi, const tuple_type &t)
    {
        Head head;
        tuple<Tail ...> tail;
        tie(head, tail) = _split_tuple(t, make_index_sequence<sizeof...(Tail)>());
        wi << head;
        dbus_tuple<Tail ...>::_from_tuple(wi, tail);
    }

    template <size_t... Ns>
    static tuple<Head, tuple<Tail ...>> _split_tuple(const tuple_type &t, index_sequence<Ns...> /*indexes*/)
    {
        return {get<0>(t), make_tuple(get<Ns+1>(t)...)};
    }
};

template<typename T>
struct dbus_tuple<T>
{
public:
protected:
    using tuple_type = tuple<T>;

    static string _sig()
    {
        return DynamicProperty<T>::sig();
    }

    static tuple_type _to_tuple(DBus::MessageIter& ri)
    {
        T t;
        ri >> t;
        return {t};
    }

    static void _from_tuple(DBus::MessageIter& wi, const tuple_type &t)
    {
        wi << get<0>(t);
    }
};

#endif /* __MDK_LIB_TUPLE__ */
